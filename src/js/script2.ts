import React from "react";
import ReactDOM from "react-dom";
import App from "../react_js/app.tsx";

function component(text: string): HTMLElement {
  const element = document.createElement("h1");
  element.textContent = text;
  return element;
}

document.body.prepend(component("PROJECT BUILT WITH WEBPACK"));

const image = document.getElementById("first_image") as HTMLImageElement;
const br = document.createElement("br");

ReactDOM.render(React.createElement(App), document.getElementById("root"));
