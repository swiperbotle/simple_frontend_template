import React from "react";
import ReactDOM from "react-dom";
import App from "../react_js/app.tsx";
function component(text) {
  const element = document.createElement("h1");
  element.textContent = text;
  return element;
}

document.body.prepend(component("PROJECT BUILDED WITH WEBPACK"));
const image = document.getElementById("first_image");
const br = document.createElement("br");
ReactDOM.render(<App />, document.getElementById("root"));
