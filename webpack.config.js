const PugPlugin = require("pug-plugin"); // pug has vulnerabilities, use carefully
const path = require("path");
const FileManagerPlugin = require("filemanager-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin"); //put css in distinct file
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin");

const plugins = [
  new PugPlugin({
    pretty: true, // formatting HTML, useful for development mode
    js: {
      // output filename of extracted JS file from source script
      filename: "bundle.[name].[contenthash:8].js",
    },
    css: {
      // output filename of extracted CSS file from source style
      filename: "styles.[name].[contenthash:8].css",
    },
  }),
  new FileManagerPlugin({
    events: {
      onStart: {
        delete: ["dist"],
      },
      onEnd: {
        copy: [
          {
            source: path.join("src", "static"), // copy some static stuff to dest, like CEO files
            destination: "dist",
          },
        ],
      },
    },
  }),
];

if (process.env.SERVE) {
  plugins.push(new ReactRefreshWebpackPlugin());
}
module.exports = {
  output: {
    path: path.join(__dirname, "dist/"),
    publicPath: "/",
  },
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
  },
  entry: {
    index: "./src/html/index.pug", // => dist/index.html
    another: "./src/html/another_index.pug",
  },
  plugins,
  module: {
    rules: [
      {
        test: /\.(ts|js)x?$/i,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            cacheDirectory: true,
          },
        },
      },
      {
        test: /\.pug$/,
        loader: PugPlugin.loader,
      },
      {
        test: /\.(scss|css)$/,
        use: ["css-loader", "postcss-loader", "sass-loader"],
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/i,
        type: "asset/resource",
        generator: {
          filename: path.join("images", "[name].[contenthash][ext]"),
        },
      },
      {
        test: /\.svg$/,
        type: "asset/resource",
        generator: {
          filename: path.join("icons", "[name].[contenthash][ext]"),
        },
      },
    ],
  },

  devServer: {
    static: { directory: path.join(__dirname, "dist") }, // live server, now stores files in this directory, temporarily
    watchFiles: path.join(__dirname, "src"),
    port: 9000,
  },

  optimization: {
    runtimeChunk: "single",
    splitChunks: {
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/].+\.(js|ts)$/,
          name: "vendor",
          chunks: "all",
        },
      },
    },
  },
};
